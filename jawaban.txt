1. Membuat database

   create database myshop;

2. Membuat table di dalam database

   create table users(
    -> id integer(8) auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

   create table categories(
    -> id integer(8) auto_increment primary key,
    -> name varchar(255)
    -> );
   
   create table items(
    -> id integer(8) auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price integer(11),
    -> stock integer(10),
    -> category_id integer(8),
    -> foreign key(category_id) references categories(id)
    -> );   
   
3. Memasukkan data pada table 

   insert into users(name, email, password)
    -> values("John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123");
   
   insert into categories(name)
    -> values("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("woman"),
    -> ("branded");
   
   insert into items(name, description, price, stock, category_id)
    -> values("Sumsang b50", "hape keren merek sumsang", 4000000, 100, 1),
    -> ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10,1);
   
4. Mengambil data dari database
   a. Mengambil data users
      mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya
      
      select id, name, email from users;
		
   b. Mengambil data items
      - mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta)
              
        select * from items where price > 1000000;

      - mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci 		  		�uniklo�, �watch�, atau �sang� (pilih salah satu saja)
        
        select * from items where name like "uniklo%";

   c. Menampilkan data items join dengan kategori
      menampilkan data items yang dilengkapi dengan data nama kategori di masing-masing items (gunakan join)
      
      select items.name, items.description, items.price, items.stock, items.category_id, categories.name as categories       from items inner join categories on items.category_id = categories.id;

5. Mengubah Data dari database
   Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000
   
   select items.name, sum(items.stock) as total_stock from items inner join categories on items.category_id    =    categories.id group by categories.name;
